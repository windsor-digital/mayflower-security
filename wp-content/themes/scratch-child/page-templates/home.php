<?php
/**
 * Template Name: Home Page
 */

get_header(); ?>

<div id="main-content" class="main-content">

<?php
if ( is_front_page() && fw_theme_has_featured_posts() ) {
	fw()->extensions->get('Home')->render_slider(10, array(
    'width'  => 1500,
    'height' => 380
));
	}
?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					// Include the page content template.
					get_template_part( 'content', 'page' );

				endwhile;
			?>
		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php
get_sidebar();
get_footer();
