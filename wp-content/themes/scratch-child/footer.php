<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 */
?>

		</div><!-- #main -->
		</div><!-- #page -->
		<footer id="colophon" class="site-footer" role="contentinfo">

			<?php get_sidebar( 'footer' ); ?>

			<div class="site-info">
                <div class="footer-wrapper">
	                <div class="footer-left">
		                <div class="footer-nav">
			                <a href="/privacy" class="util-footer">Privacy Policy</a>&nbsp; | &nbsp;       
			                <span>
				                <a href="/about-us/contact-us/" class="util-footer">Contact Us</a>&nbsp; | &nbsp;
				                <a href="/claim-form" class="util-footer">Claim Form</a>&nbsp; | &nbsp;
				                <a href="/site-map" class="util-footer">Site Map</a>
			                </span>
							<div class="footer-copyright">&copy; <?php echo date('Y'); ?> Security Van Lines</div>
						</div>
	                </div>
	                <div class="footer-right">
	                	<div class="footer-logo"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/logo-footer.png" /></div>
		                <ul class="footer-social">
			                <li><a href="https://www.facebook.com/secvanlines"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/icon-facebook.png" class="footer-social-icon" /></a></li>
			                <li><a href="https://twitter.com/securityvanline"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/icon-twitter.png" class="footer-social-icon" /></a></li>
			                <li><a href="https://www.linkedin.com/company/security-van-lines"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/icon-linkedin.png" class="footer-social-icon" /></a></li>
		                </ul>
		                
	                </div>
                </div>
			</div><!-- .site-info -->
		</footer><!-- #colophon -->
	
	<?php wp_footer(); ?>
</body>
</html>