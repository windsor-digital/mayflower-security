<?php
/**
 * The template used for displaying page content
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">
	
	<header class="entry-header">
	<?php
	if( !is_front_page() && function_exists('fw_ext_breadcrumbs_render') && is_page() ) {
		echo fw_ext_breadcrumbs_render();
	}
	?>
    <?php
		// Page title.
		the_title( '<h1 class="entry-title">', '</h1>' );
	?>
	</header><!-- .entry-header -->
		<?php
		the_content();
			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'unyson' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
			) );

			edit_post_link( __( 'Edit', 'unyson' ), '<span class="edit-link">', '</span>' );
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
