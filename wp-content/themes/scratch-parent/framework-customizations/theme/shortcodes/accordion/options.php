<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'content'      => array(
		'type'    => 'group',
		'options' => array(
			'title'    => array(
				'type'  => 'text',
				'label' => __( 'Accordion Title', 'fw' ),
				'desc'  => __( 'Write the heading title content', 'fw' ),
			),
		)
	),
	'tabs' => array(
		'type'          => 'addable-popup',
		'label'         => __( 'Tabs', 'fw' ),
		'popup-title'   => __( 'Add/Edit Tabs', 'fw' ),
		'desc'          => __( 'Create your tabs', 'fw' ),
		'template'      => '{{=tab_title}}',
		'popup-options' => array(
			'tab_title'   => array(
				'type'  => 'text',
				'label' => __('Title', 'fw')
			),
			'tab_content' => array(
				'type'   => 'wp-editor',
				'teeny'  => false,
				'reinit' => true,
				'label' => __('Content', 'fw')
			)
		)
	)
);