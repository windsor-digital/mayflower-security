<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var array $atts
 */

if( empty($atts['title']) ) {
	return;
}
?>

<?php if( empty($atts['subtitle']) ) : ?>
	<div class="shortcode-container arrowblock">
		<div class="<?php echo $atts['icon'] ?>"></div> 
		<div class="titleblock">
			<a href="<?php echo $atts['headline_link'] ?>"><span class="title"><?php echo $atts['title'] ?></span></a>
		</div>
	</div>
<?php endif ?>
<?php if( !empty($atts['subtitle']) ) : ?>
	<div class="shortcode-container arrowblock">
		<div class="<?php echo $atts['icon'] ?>"></div>
		<div class="titleblock">
			<a href="<?php echo $atts['headline_link'] ?>"><span class="title"><?php echo $atts['title'] ?></span></a>
			<span class="subtitle-article shortcode-container"><?php echo $atts['subtitle'] ?></span>
		</div>
	</div>
<?php endif ?>