<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'layout_builder' => array(
		'title'       => __( 'Circular Image Block', 'fw' ),
		'description' => __( 'Circular image on side', 'fw' ),
		'tab'         => __( 'Content Elements', 'fw' ),
	)
);
