<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

class FW_Shortcode_Toggle extends FW_Shortcode {

	protected function handle_shortcode( $atts, $content, $tag ) {
		wp_enqueue_script(
			'shortcode-toggle',
			$this->get_uri() . '/static/js/scripts.js',
			array( 'jquery-ui-toggle' ),
			fw()->theme->manifest->get_version(),
			true
		);

		return fw_render_view( $this->get_path() . '/views/view.php', array(
			'atts'    => $atts,
			'content' => $content,
			'tag'     => $tag
		) );
	}
}