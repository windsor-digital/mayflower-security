<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
	'layout_builder' => array(
		'title'       => __( 'Map', 'unyson' ),
		'description' => __( 'A very awesome map', 'unyson' ),
		'tab'         => __( 'Content Elements', 'unyson' )
	)
);