<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
} ?>

<div class="info shortcode-container">
	<a href="<?php echo $atts['button_link'] ?>" class="button <?php echo $atts['button_featured'] ?>"
	   target="<?php echo $atts['button_target'] ?>"><?php echo $atts['button_label'] ?></a>
</div>