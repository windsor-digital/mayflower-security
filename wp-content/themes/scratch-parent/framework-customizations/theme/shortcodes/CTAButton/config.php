<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
	'layout_builder' => array(
		'title'       => __( 'CTA Button', 'fw' ),
		'description' => __( 'Create CTA Button', 'fw' ),
		'tab'         => __( 'Content Elements', 'fw' )
	)
);