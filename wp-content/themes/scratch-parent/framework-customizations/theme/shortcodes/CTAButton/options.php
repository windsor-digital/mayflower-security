<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'button_featured' => array(
        'type'  => 'switch',
		'label'   => __( 'Featured Button', 'fw' ),
		'desc'    => __( 'Select here if this is a featured CTA', 'fw' ),
        'right-choice' => array(
            'value' => 'featured',
            'label' => __('Yes', 'fw'),
        ),
        'left-choice' => array(
            'value' => 'plain',
            'label' => __('No', 'fw'),
        ),
    ),
	'button_label'  => array(
		'label' => __( 'Button Label', 'fw' ),
		'desc'  => __( 'This is the text that appears on your button', 'fw' ),
		'type'  => 'text',
		'value' => 'Click'
	),
	'button_link'   => array(
		'label' => __( 'Button Link', 'fw' ),
		'desc'  => __( 'Where should your button link to', 'fw' ),
		'type'  => 'text',
		'value' => '#'
	),
	'button_target' => array(
        'type'  => 'switch',
		'label'   => __( 'Open Link in New Window', 'fw' ),
		'desc'    => __( 'Select here if you want to open the linked page in a new window', 'fw' ),
        'right-choice' => array(
            'value' => '_blank',
            'label' => __('Yes', 'fw'),
        ),
        'left-choice' => array(
            'value' => '_self',
            'label' => __('No', 'fw'),
        ),
    ),
);